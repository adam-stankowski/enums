package as.enums;

import java.security.InvalidParameterException;

/**
 * 
 * @author Adam Stankowski
 * This is an enum which behaves differently depending on its own value
 * It uses an abstract function which is implemented by each enum value. 
 * This is flexible when we want to add a new value and make sure 
 * we don't forget to process an associated operation as it will give 
 * us a compile time error when we do so.
 * Tax calculations are totally hypothetical.
 *
 */
public enum Tax {
  NEGATIVE(0.02) { 
    @Override double apply(double amount){
      if(amount <= TAX_FREE_AMOUNT) 
        return amount * (1 - getTaxAmount()); 
      else 
        return amount;
      }
    },
  ZERO(0) {
      @Override double apply(double amount){
        return amount;
      }
    },
  REDUCED(0.23) {
      @Override double apply(double amount) {
        if(amount <= TAX_FREE_AMOUNT)
          return amount;
        else
          return 
              amount + (amount - TAX_FREE_AMOUNT) * (1 + getTaxAmount());
    }
  },
  FULL(0.23){
    @Override
    double apply(double amount) {
      return amount * (1 + getTaxAmount());
    }
  };
  
  private final double taxAmount;  
  private static final double TAX_FREE_AMOUNT = 500;
  Tax(double taxAmount){
    if(taxAmount >= 1){
      throw new InvalidParameterException("Tax cannot be 100%");
    }
    this.taxAmount = taxAmount;
  }
  
  /**
   * Describes how much I should pay for certain net amount after 
   * applying a given tax
   * @param amount net amount to be calculated
   * @return
   */
  abstract double apply(double amount);
  
  double getTaxAmount(){
    return taxAmount;
  }
}
