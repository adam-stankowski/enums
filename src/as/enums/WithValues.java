package as.enums;

public enum WithValues {
  SPRING(1),
  SUMMER(2),
  AUTUMN(3),
  WINTER(4);
  
  public final int type;
  WithValues(int type){
    this.type = type;
  }
}
