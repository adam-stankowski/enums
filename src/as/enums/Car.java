/**
 * 
 */
package as.enums;

import java.security.InvalidParameterException;
import java.util.ArrayList;

/**
 * @author Adam Stankowski
 *  enum Car presents a rich enum type.
 */
public enum Car {
  CITY(4,40),
  SEDAN(5,300),
  ESTATE(5,600),
  SUV(5,700),
  VAN(2,3000);
  
  private final int passengers;
  private final int luggageSpaceLitres;
  private static final int AVG_PERSON_WEIGHT_KG = 80;
  
  Car(int passengers, int luggage){
    this.passengers = passengers;
    this.luggageSpaceLitres = luggage;
  }
  public int getPassengers() {
    return passengers;
  }
  public int getLuggageSpaceLitres() {
    return luggageSpaceLitres;
  }
  
  /**
   * Function returning the total weight of every enumerated car.
   * @return
   */
  public double totalWeight(){
    return passengers * AVG_PERSON_WEIGHT_KG + luggageSpaceLitres;
  }
  
}
