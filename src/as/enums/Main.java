package as.enums;

public class Main {

  public static void main(String[] args) {
    Car c = Car.ESTATE;
    System.out.println(c);
    
    for(Car car: Car.values()){
      System.out.printf("Weight of %s is %.2f%n",car.toString(),car.totalWeight());
    }
    System.out.println();
    for(Tax tax: Tax.values()){
      System.out.printf("For net price of %.2f with tax %s I should pay %.2f%n", 550.0, tax, tax.apply(550));
    }
    System.out.println();
    for(Tax tax: Tax.values()){
      System.out.printf("For net price of %.2f with tax %s I should pay %.2f%n", 200.0, tax, tax.apply(200));
    }
    
    System.out.println("Spring "+ WithValues.SPRING.type);
    //WithValues.SPRING.type = 7; - checking if it can be assigned :)
    // final field cannot but it's probably better to use a public getter and keep type private.
    
  }

}
